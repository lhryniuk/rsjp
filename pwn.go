package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

func retrieveTranslation(s *goquery.Selection) string {
	str, err := s.Html()
	if err != nil {
		log.Fatal(err)
	}
	return strings.Replace(str, "<br/>", "\n", -1)
}

func find(s *goquery.Selection) bool {
	res := false
	s.Find("b").EachWithBreak(func(i int, s *goquery.Selection) bool {
		res = strings.Contains("znaczenie:", s.Text())
		return !res
	})
	return res
}

func Sjp(word string, out chan string) {
	response, err := http.Get(fmt.Sprintf("https://www.sjp.pl/%s", word))
	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()

	doc, err := goquery.NewDocumentFromReader(response.Body)
	if err != nil {
		log.Fatal(err)
	}

	found := false
	translation := ""
	doc.Find("p").EachWithBreak(func(i int, s *goquery.Selection) bool {
		if found {
			translation = retrieveTranslation(s)
		}
		found = found || find(s)
		return len(translation) == 0
	})

	out <- translation
}
