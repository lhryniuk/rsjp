package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	if len(os.Args) != 2 {
		log.Fatal(fmt.Sprintf("usage: %s <word>\n", os.Args[0]))
	}

	word := os.Args[1]

	chs := make(map[string]chan string)
	chs["sjp"] = make(chan string)
	chs["sjp-pwn"] = make(chan string)

	go Sjp(word, chs["sjp"])
	go SjpPwn(word, chs["sjp-pwn"])

	translation := fmt.Sprintf("sjp.pl\n%s\n\nsjp.pwn.pl%s", <-chs["sjp"], <-chs["sjp-pwn"])
	fmt.Printf("%s\n\n%s\n", word, translation)
}
