package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/PuerkitoBio/goquery"
)

func SjpPwn(word string, out chan string) {
	response, err := http.Get(fmt.Sprintf("https://sjp.pwn.pl/szukaj/%s", word))
	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()

	doc, err := goquery.NewDocumentFromReader(response.Body)
	if err != nil {
		log.Fatal(err)
	}

	translation := ""
	doc.Find("div.znacz").Each(func(i int, s *goquery.Selection) {
		translation = translation + s.Text()
	})

	out <- translation
}
