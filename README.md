# rsjp
A command line Polish dictionary

## Installation

```shell
$ go get -u github.com/hryniuk/rsjp
```

## Usage

```shell
$ rsjp słownik
słownik

sjp.pl
1. zbiór wyrazów ułożonych i opracowanych według pewnej zasady, zwykle objaśnianych pod względem znaczeniowym;
2. indywidualny zasób wyrazów

sjp.pwn.pl
1. «zbiór wyrazów ułożonych i opracowanych według pewnej zasady, zwykle objaśnianych pod względem znaczeniowym»
2. «indywidualny zasób wyrazów»
```
